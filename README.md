# X3

This is the [Portmod](https://gitlab.com/portmod/portmod) package repository for X3. Currently only X3: Albion Prelude is supported.

See the Portmod [Guide](https://gitlab.com/portmod/portmod/-/wikis/home#guide) for details on how to get started.
